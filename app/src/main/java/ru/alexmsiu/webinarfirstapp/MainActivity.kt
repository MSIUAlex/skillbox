package ru.alexmsiu.webinarfirstapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        FirstButton.setOnClickListener {
           TextLable.setText("Вы нажали кнопку: " + FirstButton.text.toString());
        }
        SecondButton.setOnClickListener {
            TextLable.setText("Вы нажали кнопку: " + SecondButton.text.toString());
        }
        ThirdButton.setOnClickListener {
            TextLable.setText("Вы нажали кнопку: " + ThirdButton.text.toString());
        }
        FourthButton.setOnClickListener {
            TextLable.setText("Вы нажали кнопку: " + FourthButton.text.toString());
        }
    }

}